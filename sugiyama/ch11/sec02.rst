==================================================
シュヴァルツシルト解
==================================================

1915年に、カール・シュヴァルツシルトが求めた解。
歴史上、最初に求められたアインシュタイン方程式の厳密解。

「球対称」という仮定は、地球や太陽といった星の周りの空間が球対称な空間になっていると考えられるから、非常に有用なものだった。
そのため、この解は太陽系において一般相対性理論の検証実験を進める上で、必要不可欠。

太陽の引き起こす重力レンズ効果、水星の近日点移動や、
重力の非常に強い場合には、静止しているブラックホールを表すことができる。


シュバルツシルト解の条件
==================================================

#. 静止している質量Ｍの物体
#. 球対称な時空
#. 静的なメトリック


準備体操：極座標の微小要素
==================================================

球対称な時空を考えるので「極座標」を使う。
これは球対称な場合に便利な座標の取り方。

その微小要素は、直交座標と少し異なっているので、計算するときには気をつける。
以下、極座標変換を思い出しながら、極座標での不変間隔が教科書p.150（11.1）式の形になるのかを確かめる。


まず、極座標変換すると、

.. math::
   :nowrap:

   \begin{align}
   x & = r \sin \theta \cos \phi\\
   y & = r \sin \theta \sin \phi\\
   z & = r \cos \theta
   \end{align}

４次元時空の極座標は、以下のようにおけばよい

.. math::
   :nowrap:

   \begin{align}
   x^{\mu} & = (x^{0}, x^{1}, x^{2}, x^{3})\\
   & = (ct, r, \theta, \phi)
   \end{align}


微小要素も計算しておく。
ちなみに三角関数の微分は以下のよう。

.. math::
   :nowrap:

   \begin{align}
   \mathrm{d} ( \sin \theta ) & = \cos \theta \mathrm{d} \theta\\
   \mathrm{d} ( \cos \theta ) & = - \sin \theta \mathrm{d} \theta
   \end{align}

まず、:math:`\mathrm{d}x` から計算する。

.. math::
   :nowrap:

   \begin{align}
   x & = x(r, \theta, \phi) = r \sin \theta \cos \phi\\
   \Rightarrow \quad \mathrm{d}x
   & =
   \frac{\partial x}{\partial r} \mathrm{d}r
   + \frac{\partial x}{\partial \theta} \mathrm{d} \theta
   + \frac{\partial x}{\partial \phi} \mathrm{d} \phi\\
   & =
   \frac{\mathrm{d} r}{\mathrm{d} r} \sin \theta \cos \phi \mathrm{d}r
   + r \frac{\mathrm{d} \sin \theta }{\mathrm{d} \theta} \cos \phi \mathrm{d} \theta
   + r \sin \theta \frac{\mathrm{d} \cos \phi }{\mathrm{d} \phi} \mathrm{d} \phi\\
   & =
   \left( \sin \theta \cos \phi \right) \mathrm{d}r
   + \left( r \cos \theta \cos \phi \right) \mathrm{d} \theta
   + \left( - r \sin \theta \sin \phi \right) \mathrm{d} \phi
   \end{align}


次に、:math:`\mathrm{d}y` を計算する。

.. math::
   :nowrap:

   \begin{align}
   y & = y(r, \theta, \phi) = r \sin\theta \cos\phi \\
   \Rightarrow \quad \mathrm{d}y
   & =
   \frac{\partial y}{\partial r} \mathrm{d}r
   + \frac{\partial y}{\partial \theta} \mathrm{d} \theta
   + \frac{\partial y}{\partial \phi} \mathrm{d} \phi\\
   & =
   \frac{\mathrm{d} r}{\mathrm{d} r} \sin \theta \sin \phi \mathrm{d}r
   + r \frac{\mathrm{d} \sin \theta }{\mathrm{d} \theta} \sin \phi \mathrm{d} \theta
   + r \sin \theta \frac{\mathrm{d} \sin \phi }{\mathrm{d} \phi} \mathrm{d} \phi\\
   & =
   \left( \sin \theta \sin \phi \right) \mathrm{d}r
   + \left( r \cos \theta \sin \phi \right) \mathrm{d} \theta
   + \left( r \sin \theta \cos \phi \right) \mathrm{d} \phi
   \end{align}


最後に、:math:`\mathrm{d}z` を計算する。
:math:`z` には :math:`\phi` 成分がないので、 :math:`\mathrm{d}x` 、 :math:`\mathrm{d}y` の計算に比べるととても楽ちん。

.. math::
   :nowrap:

   \begin{align}
   z & = z(r, \theta) = r \cos \theta\\
   \Rightarrow \quad \mathrm{d}z
   & =
   \frac{\partial z}{\partial r} \mathrm{d}r
   + \frac{\partial z}{\partial \phi} \mathrm{d} \phi\\
   & =
   \frac{\mathrm{d} r}{\mathrm{d} r} \cos \theta \mathrm{d}r
   + r \frac{\mathrm{d} \cos \theta }{\mathrm{d} \theta} \mathrm{d} \theta\\
   & =
   \left( \cos \theta \right) \mathrm{d}r
   + \left( - r \sin \theta \right) \mathrm{d} \theta
   \end{align}



準備体操：極座標の不変間隔
==================================================

不変間隔 :math:`\mathrm{d}s^{2}` を極座標を使って表す。

.. math::
   :nowrap:

   \begin{align}
   \mathrm{d}s^{2} & = - (c \mathrm{d}t)^{2} + \mathrm{d}x^{2} + \mathrm{d}y^{2} + \mathrm{d}z^{2}\\
   & = - (c \mathrm{d}t)^{2}\\
   & + \left\{ \left( \sin \theta \cos \phi \right) \mathrm{d}r
   + \left(   r \cos \theta \cos \phi \right) \mathrm{d} \theta
   + \left( - r \sin \theta \sin \phi \right) \mathrm{d} \phi \right\}^{2}\\
   & + \left\{ \left( \sin \theta \sin \phi \right) \mathrm{d}r
   + \left( r \cos \theta \sin \phi \right) \mathrm{d} \theta
   + \left( r \sin \theta \cos \phi \right) \mathrm{d} \phi \right\}^{2}\\
   & + \left\{ \left( \cos \theta \right) \mathrm{d}r
   + \left( - r \sin \theta \right) \mathrm{d} \theta \right\}^{2}
   \end{align}


ちゃんと計算すると、めんどくさそうなので、ちょんぼする。
求めたいゴールから逆算する :math:`\mathrm{d}r \mathrm{d}\theta`, :math:`\mathrm{d}r \mathrm{d}\phi`, :math:`\mathrm{d}\theta \mathrm{d}\phi` の係数は0になるはずなので、:math:`\mathrm{d}r^{2}` 、 :math:`\mathrm{d}\theta^{2}` 、 :math:`\mathrm{d}\phi^{2}` の係数をそれぞれまとめることにする。

.. math::
   :nowrap:

   \begin{align}
   \mathrm{d}s^{2}
   & = - (c \mathrm{d}t)^{2}\\
   & \quad + \left( \sin \theta \cos \phi \right)^{2} \mathrm{d}r^{2}
     + \left( \sin \theta \sin \phi \right)^{2}       \mathrm{d}r^{2}
     + \left( \cos \theta \right)^{2}                 \mathrm{d}r^{2}\\
   & \quad + \left( r \cos \theta \cos \phi \right)^{2}   \mathrm{d}\theta^{2}
     + \left(   r \cos \theta \sin \phi \right)^{2} \mathrm{d}\theta^{2}
     + \left( - r \sin \theta \right)^{2}             \mathrm{d}\theta^{2}\\
   & \quad + \left( - r \sin \theta \sin \phi \right)^{2} \mathrm{d}\phi^{2}
     + \left(   r \sin \theta \cos \phi \right)^{2}       \mathrm{d}\phi^{2}\\
   & = - (c \mathrm{d}t)^{2}\\
   & \quad + \left( \sin^{2} \theta \cos^{2} \phi + \sin^{2} \theta \sin^{2} \phi + \cos^{2} \theta \right) \mathrm{d}r^{2}\\
   & \quad + \left( r^{2} \cos^{2} \theta \cos^{2} \phi + r^{2} \cos^{2} \theta \sin^{2} \phi + r^{2} \sin^{2} \theta \right) \mathrm{d}\theta^{2}\\
   & \quad + \left( r^{2} \sin^{2} \theta \sin^{2} \phi + r^{2} \sin^{2} \theta \cos^{2} \phi \right)     \mathrm{d}\phi^{2}\\
   & = - (c \mathrm{d}t)^{2}\\
   & \quad + \left( \sin^{2} \theta ( \cos^{2} \phi + \sin^{2} \phi ) + \cos^{2} \theta \right) \mathrm{d}r^{2}\\
   & \quad + r^{2} \left( \cos^{2} \theta ( \cos^{2} \phi + \sin^{2} \phi ) + \sin^{2} \theta \right) \mathrm{d}\theta^{2}\\
   & \quad + \left( r^{2} \sin^{2} \theta ( \sin^{2} \phi + \cos^{2} \phi ) \right)     \mathrm{d}\phi^{2}\\
   & = - (c \mathrm{d}t)^{2}\\
   & \quad + \left( \sin^{2} \theta + \cos^{2} \theta \right) \mathrm{d}r^{2}\\
   & \quad + r^{2} \left( \cos^{2} \theta + \sin^{2} \theta \right) \mathrm{d}\theta^{2}\\
   & \quad + \left( r^{2} \sin^{2} \theta \right)     \mathrm{d}\phi^{2}\\
   & = - (c \mathrm{d}t)^{2}\\
   & \quad + \mathrm{d}r^{2}\\
   & \quad + r^{2} \mathrm{d}\theta^{2}\\
   & \quad + r^{2} \sin^{2} \theta \mathrm{d}\phi^{2}\\
   \mathrm{d}s^{2}
   & = - (c \mathrm{d}t)^{2}
   + \mathrm{d}r^{2}
   + r^{2} \mathrm{d}\theta^{2}
   + r^{2} \sin^{2} \theta \mathrm{d}\phi^{2}
   \end{align}


準備体操：極座標のメトリック
==================================================

直交座標での不変間隔とメトリックはそれぞれ以下のようだった。

.. math::
   :nowrap:

   \begin{align}
   \mathrm{d}s^{2} & = - (c \mathrm{d}t)^{2} + \mathrm{d}x^{2} + \mathrm{d}y^{2} + \mathrm{d}z^{2}\\
   (g_{\mu \nu}) & = (-1, 1, 1, 1)
   \end{align}


なので、極座標表示の場合のメトリックは、以下のようになる。

.. math::
   :nowrap:

   \begin{align}
   \mathrm{d}s^{2} & = - (c \mathrm{d}t)^{2} + \mathrm{d}r^{2} + r^{2} \mathrm{d} \theta^{2} + r^{2} \sin^{2} \theta \mathrm{d} \phi^{2}\\
   (g_{\mu \nu}) & = (-1, 1, r^{2}, r^{2} \sin^{2} \theta)
   \end{align}


シュヴァルツシルト解の条件を定性的に考える
==================================================

もういちど、シュヴァルツシルト解の条件を整理する。

#. 空間座標の原点に、質量Mの物体（例：恒星）が静止している
#. 時空は球対称
#. メトリックは静的（＝時間に依存しない）
#. 物体から十分離れたところでは、質量Mの影響がない。つまり、メトリックはミンコフスキー時空に一致しなければならない

このような条件をみたす、最も単純なメトリックを考える。
そうすると、メトリックの成分のうち、定数であるものをrの関数とするのがよさそう。

.. math::
   :nowrap:

   \begin{align}
   g_{\mu \nu} & = (-1, 1, r^{2}, r^{2} \sin^{2} \theta)\\
   & \Rightarrow (g_{00}(r), g_{11}(r), r^{2}, r^{2} \sin^{2} \theta)
   \end{align}


不変間隔 :math:`\mathrm{d}s^{2}` は以下のようになる。

.. math::
   :nowrap:

   \begin{align}
   \mathrm{d}s^{2} & = g_{00}(r) (c \mathrm{d}t)^{2} + g_{11}(r) \mathrm{d}r^{2} + r^{2} \mathrm{d} \theta^{2} + r^{2} \sin^{2} \theta \mathrm{d} \phi^{2}
   \end{align}


次は、この :math:`g_{00}(r)`, :math:`g_{11}(r)` がどのような形をしているべきかを考える。
遠方でミンコフスキー時空のメトリックと一致するということは、
:math:`g_{00}(r) \to -1` 、
:math:`g_{11}(r) \to 1`
ということなので、
:math:`g_{00} < 0`, :math:`g_{11} > 0` である。
:math:`r` のみの関数である :math:`\nu(r)` と :math:`\mu(r)` を勝手に作って、
:math:`g_{00} = -e^{\nu}` 、
:math:`g_{11} = e^{\mu}`
と置き換えると、メトリック（教科書p.150（11.2）式）は以下の形（教科書p.151（11.3）式）を得る。

.. math::
   :nowrap:

   \begin{align}
   g_{\mu \nu} & = (-e^{\nu}, e^{\mu}, r^{2}, r^{2} \sin^{2} \theta)
   \end{align}

このメトリックを使ってクリストッフェル記号を求める。


クリストッフェル記号の計算
==================================================

クリストッフェル記号はなんだったかを思い出すには、教科書p.126（9.33）式のあたりを確認する。
メトリックから導かれる接続のことを、特にクリストッフェル記号と呼ぶそうだ。
クリストッフェル記号はメトリックの一階微分で求めることができる。

.. math::
   :nowrap:

   \begin{align}
      \Gamma^{\mu}_{\nu\lambda} & = \frac{1}{2} g^{\mu \kappa} \left(
        \partial_{\lambda} g_{\kappa \nu}
      + \partial_{\nu} g_{\kappa \lambda}
      - \partial_{\kappa} g_{\lambda \nu}
      \right)
   \end{align}
