==================================================
曲率（リーマン曲率テンソル）
==================================================

空間の曲がり方は、ベクトルを別の経路で平行移動させたときの **差** として定量化できる。
その差を **リーマン曲率テンソル** と呼ぶ。

空間が平坦な場合、リーマン曲率テンソル＝０になる。
（正の場合は山に、負の場合は谷になってるのかな？）


.. important::

   .. math::

      R^{\mu}_{\nu \lambda \kappa}
      = \partial_{\lambda} \Gamma^{\mu}_{\nu \kappa}
      - \partial_{\kappa} \Gamma^{\mu}_{\nu \lambda}
      + \Gamma^{\mu}_{\eta \lambda} \Gamma^{\eta}_{\nu \kappa}
      - \Gamma^{\mu}_{\eta \kappa} \Gamma^{\eta}_{\nu \lambda}



リーマン曲率テンソル
==================================================

リーマン曲率テンソルは **空間（時空）の曲がり具合を表す指標** となる値である。
これはベクトルを平行移動させたときに、空間が曲がっていると移動の順番によって差が生じるよ、ということ。
どうしてこで生じるかのイメージは 杉山本 p.123 図9.2 を見ながら考えるとよい。
空間が平坦であれば、差は生じないので、リーマン曲率テンソルは常に０である。

ランク４のテンソルで、上付き添字が１つ、下付き添字が３つである。
３階共変・１階反変テンソル、というらしい。
**クリストッフェル記号** を使って以下のように表すことができる。

.. math::
   :nowrap:

   \begin{align}
   R^{\mu}_{\nu \lambda \kappa}
   & = \partial_{\lambda} \Gamma^{\mu}_{\nu \kappa}
   - \partial_{\kappa} \Gamma^{\mu}_{\nu \lambda}
   + \Gamma^{\mu}_{\eta \lambda} \Gamma^{\eta}_{\nu \kappa}
   - \Gamma^{\mu}_{\eta \kappa} \Gamma^{\eta}_{\nu \lambda}
   \end{align}

４階のテンソル。
添字の位置を見ると、１階の反変成分と３階の共変成分を持っている。

:math:`\mu, \nu, \lambda, \kappa` = ０〜３（＝４成分）なので、
単純に考えると、リーマン曲率テンソルの成分は :math:`4 \times 4 \times 4 \times 4 = 246` 個ある。
ただし、以下の対称性とかを考慮することで **独立な成分は20個** まで減少する。
（対称性については :doc:`第9.5節 <../sugiyama/09-5>` を参照）


平行移動の経路１（ :math:`x + \mathrm{d}x + \delta x` ）
============================================================

まず、 :math:`x \to x + \mathrm{d}x` への平行移動

.. math::
   :nowrap:

   \begin{align}
   \overline{V}^{\mu}(x + \mathrm{d}x) & = V^{\mu}(x) - \Gamma^{\mu}_{\nu \lambda}(x) V^{\nu}(x) \mathrm{d}x^{\lambda}
   \end{align}

次に :math:`x + \mathrm{d}x \to x + \mathrm{d}x + \delta x` への平行移動。
上の式を見ながら、以下の置換えを考えればよい

.. math::
   :nowrap:

   \begin{align}
   x & \to x + \mathrm{d}x\\
   \mathrm{d}x & \to \delta x
   \end{align}

.. math::
   :nowrap:

   \begin{align}
   \overline{V}^{\mu}((x + \mathrm{d}x) + \delta x) & = V^{\mu}(x + \mathrm{d}x) - \Gamma^{\mu}_{\nu \lambda}(x + \mathrm{d}x) V^{\nu}(x + \mathrm{d}x) \delta x^{\lambda}
   \end{align}

と思ったんだけど、教科書（p.124）の式（9.23）を見ると、右辺の :math:`V` が :math:`\overline{V}` なってるのはなんでだっけ？


テイラー展開
==================================================

YouTubeにて解説動画を発見（https://www.youtube.com/watch?v=3AbiJ8cMVtU）


ある関数を **べき級数** で近似する方法。
つまり、（１乗の項）＋（２乗の項）＋（３乗の項）＋・・・、という風に足し算で近似していく方法。
ａ＝０の時を、特別に **マクローリン展開** と呼ぶ。

.. math::
   :nowrap:

   \begin{align}
   f(x) & = \sum^{\infty}_{n=0} \frac{f^{(n)}(a)}{n!}(x-a)^{n}
   \end{align}


:math:`\Gamma^{\mu}_{\nu \lambda}(x + \mathrm{d}x)` をテイラー展開する場合、
:math:`a = \mathrm{d}x` と置いて、上のテイラー展開の式に当てはめる。

.. math::
   :nowrap:

   \begin{align}
   f(x) & = \sum^{\infty}_{n=0} \frac{f^{(n)}(a)}{n!}(x-a)^{n}
   \end{align}




平行移動の経路２（ :math:`x + \mathrm{d}x + \delta x` ）
============================================================
