==================================================
測地線方程式
==================================================

.. admonition:: 教科書

   p.127


.. important::

   .. math::
      :nowrap:

      \begin{equation}
      \begin{split}
      \qq{空間的} \dv[2]{x^{\mu}}{s} + \Gamma^{\mu}_{\nu \lambda} \dv{x^{\nu}}{s} \dv{x^{\lambda}}{s} &= 0\\
      \qq{時間的} \dv[2]{x^{\mu}}{\tau} + \Gamma^{\mu}_{\nu \lambda} \dv{x^{\nu}}{\tau} \dv{x^{\lambda}}{\tau} &= 0
      \end{split}
      \end{equation}

   .. math::
      :nowrap:

      \begin{equation}
      \begin{split}
      \qq{ヌル} \dv[2]{x^{\mu}}{\lambda} + \Gamma^{\mu}_{\kappa \nu} \dv{x^{\kappa}}{\lambda} \dv{x^{\nu}}{\lambda} &= 0\\
      \qq{ただし} g_{\mu}{\nu} \dv{x^{\mu}}{\lambda} \dv{x^{\nu}}{\lambda} &= 0
      \end{split}
      \end{equation}

測地線は **2点を結ぶ最短距離となる経路** のことである。
**測地線方程式** は、重力場が存在する場合に、質点や光が重力の影響を受けてどのような経路を取るのかを表す方程式。

まず、空間的（space-like）に離れている2点間の不変間隔が、最短となる条件を **最小作用の原理** を使って求める。

.. todo::
   最小作用の原理について、別項目にまとめる


最小作用の原理を使うと **オイラー・ラグランジュ方程式** を求めることができる。
この方程式をこちょこちょ変形して測地線方程式を求めることができる。
:math:`\dot{x}` のように上に付いている :math:`\dot{}` （ドット）は時間微分を意味していて、
:math:`\ddot{}` （ドットが2つ）は2階の時間微分を意味している。

.. math::

   \ddot{x}^{\mu} + \Gamma^{\mu}_{\nu \lambda} \dot{x}^{\nu} \dot{x}^{\lambda} = 0

.. todo::

   測地線方程式を求める計算過程を別項目にまとめる


測地線方程式を、空間的（）、時間的（）、ヌル（）な場合に分けて書くと、以下のようになる。


.. math::
   :nowrap:

   \begin{align}
   \frac{\mathrm{d}^{2}x^{\mu}}{\mathrm{d}s^{2}} + \Gamma^{\mu}_{\nu \lambda} \frac{\mathrm{d}x^{\nu}}{\mathrm{d}s}\frac{\mathrm{d}x^{\lambda}}{\mathrm{d}s} & = 0 \quad \textrm{(space-like)}\\
   \frac{\mathrm{d}^{2}x^{\mu}}{\mathrm{d}\tau^{2}} + \Gamma^{\mu}_{\nu \lambda} \frac{\mathrm{d}x^{\nu}}{\mathrm{d}\tau}\frac{\mathrm{d}x^{\lambda}}{\mathrm{d}\tau} & = 0 \quad \textrm{(time-like)}\\
   \frac{\mathrm{d}^{2}x^{\mu}}{\mathrm{d}\lambda^{2}} + \Gamma^{\mu}_{\kappa \nu} \frac{\mathrm{d}x^{\kappa}}{\mathrm{d}\lambda}\frac{\mathrm{d}x^{\nu}}{\mathrm{d}\lambda} & = 0 \quad \textrm{(null)}
   \end{align}

.. todo::

   ヌル、の測地線方程式を求める計算過程・思考の過程を別項目にまとめる


与えられた重力場中の **テスト粒子の運動** を記述する方程式なので、重力場の理論にしなきゃいけない。

============================================================
10分補講：測地線方程式を用いたクリストッフェル記号の計算
============================================================


.. admonition:: 教科書

   p.131


測地線方程式には、質点や光の経路を決めるという物理的な意味の他に、
クリストッフェル記号の成分が **０でないもの／０であるもの** を簡単に計算できるというご利益があるらしい。
一般相対性理論で対象とする時空は、高い対称性を持っているため、クリストッフェル記号の成分の多くは０になるそう。
逆に対称性を仮定しないと解けないっぽい。




2次元球面の例題
============================================================

10分補講にある **2次元球面（=3次元球の表面）の例題** に沿って実際に計算をしてみて体感してみる。

.. danger::

   「10分補講」と付いているけど、やっぱり10分では絶対に終わらない計算量でした


3次元球の表面を考えるので **極座標** を使うことにする。
3次元空間の不変間隔を極座標を使って表すと以下のようになる。

.. math::
   :nowrap:

   \begin{align}
   \mathrm{d}s^{2} & =  \mathrm{d}x^{2} + \mathrm{d}y^{2} + \mathrm{d}z^{2}\\
   \Rightarrow \mathrm{d}s^{2} & =  \mathrm{d}r^{2} + r^{2}\mathrm{d}\theta^{2} + r^{2}\sin^{2}\theta \mathrm{d}\phi^{2}
   \end{align}

.. todo::

   極座標変換の計算を別項目にまとめる


3次元球とは、半径 :math:`r = a` と一定の値を取る場合のことなので、 微少量 :math:`\mathrm{d}r = 0` となる。
これらを代入すると、

.. math::

   \mathrm{d}s^{2} = a^{2}\mathrm{d}\theta^{2} + a^{2}\sin^{2}\theta \mathrm{d}\phi^{2}

このときの2次元座標を :math:`(x^{1}, x^{2}) = (\theta, \phi)` とすると、メトリック :math:`g_{ij}` は次の成分を持つことになる。

.. math::
   :nowrap:

   \begin{equation}
   \begin{split}
   (g_{ij}) & =
   \begin{pmatrix}
   a^{2} & 0\\
   0 & a^{2} \sin^{2} \theta
   \end{pmatrix}
   \end{split}
   \end{equation}


.. hint::

   この計算は不変間隔の定義式から逆算する感じ


オイラー・ラグランジュ方程式を成分ごとに計算する
==================================================

測地線方程式を導出するときに使ったオイラー・ラグランジュ方程式を成分ごとに計算し、
その独立な方程式が与える係数から **0でないクリストッフェル記号を求める** ことができる。

まず、ラグランジアンを計算する。
ただし、ここで使うラグランジアンは :math:`\tilde{L} \equiv L^{2}` と、 :math:`L` （＝不変間隔）の２乗で置き換えたもの。
これは、不変間隔のままだと二乗根が付いてきて、計算がめんどくさいため。


.. math::
   :nowrap:

   \begin{equation}
   \begin{split}
   \tilde{L} \equiv L^{2}
   & = g_{\mu \nu} \frac{\mathrm{d}x^{\mu}}{\mathrm{d}\lambda} \frac{\mathrm{d}x^{\nu}}{\mathrm{d}\lambda}\\
   & = \sum_{i=1,2} \sum_{j=1,2} g_{ij} \frac{\mathrm{d}x^{i}}{\mathrm{d}\lambda} \frac{\mathrm{d}x^{j}}{\mathrm{d}\lambda}\\
   & = g_{11} \frac{\mathrm{d}x^{1}}{\mathrm{d}\lambda} \frac{\mathrm{d}x^{1}}{\mathrm{d}\lambda}
   + g_{12} \frac{\mathrm{d}x^{1}}{\mathrm{d}\lambda} \frac{\mathrm{d}x^{2}}{\mathrm{d}\lambda}
   + g_{21} \frac{\mathrm{d}x^{2}}{\mathrm{d}\lambda} \frac{\mathrm{d}x^{1}}{\mathrm{d}\lambda}
   + g_{22} \frac{\mathrm{d}x^{2}}{\mathrm{d}\lambda} \frac{\mathrm{d}x^{2}}{\mathrm{d}\lambda}\\
   & = a^{2} \frac{\mathrm{d}x^{1}}{\mathrm{d}\lambda} \frac{\mathrm{d}x^{1}}{\mathrm{d}\lambda}
   + 0 \frac{\mathrm{d}x^{1}}{\mathrm{d}\lambda} \frac{\mathrm{d}x^{2}}{\mathrm{d}\lambda}
   + 0 \frac{\mathrm{d}x^{2}}{\mathrm{d}\lambda} \frac{\mathrm{d}x^{1}}{\mathrm{d}\lambda}
   + a^{2} \sin^{2} \theta \frac{\mathrm{d}x^{2}}{\mathrm{d}\lambda} \frac{\mathrm{d}x^{2}}{\mathrm{d}\lambda}\\
   & = a^{2} \frac{\mathrm{d} \theta}{\mathrm{d}\lambda} \frac{\mathrm{d} \theta}{\mathrm{d}\lambda}
   + a^{2} \sin^{2} \theta \frac{\mathrm{d}\phi}{\mathrm{d}\lambda} \frac{\mathrm{d}\phi}{\mathrm{d}\lambda}\\
   \end{split}
   \end{equation}


.. math::
   :nowrap:

   \begin{equation}
   \therefore \quad \tilde{L}
   = a^{2} \dot{\theta}^{2}
   + a^{2} \sin^{2} \theta \dot{\phi}^{2}
   \end{equation}


これをオイラー・ラグランジュ方程式に代入して、 :math:`\mu = 1, 2` の2成分を計算する。
上で求めたように :math:`\tilde{L}` には三角関数も入っているので、各成分についてゆっくり計算する。


オイラー・ラグランジュ方程式

.. math::
   :nowrap:

   \begin{equation}
   \frac{\mathrm{d}}{\mathrm{d}\lambda} \frac{\partial \tilde{L}}{\partial \dot{x}^{\mu}} - \frac{\partial \tilde{L}}{\partial x^{\mu}} = 0
   \end{equation}


オイラー・ラグランジュ方程式の :math:`\mu=1` 成分の計算

.. math::
   :nowrap:

   \begin{equation}
   \begin{split}
   \frac{\mathrm{d}}{\mathrm{d}\lambda} \frac{\partial \tilde{L}}{\partial \dot{\theta}} - \frac{\partial \tilde{L}}{\partial \theta} = 0
   \end{split}
   \end{equation}


オイラー・ラグランジュ方程式の :math:`\mu=2` 成分の計算

.. math::
   :nowrap:

   \begin{equation}
   \begin{split}
   \frac{\mathrm{d}}{\mathrm{d}\lambda} \frac{\partial \tilde{L}}{\partial \dot{\phi}} - \frac{\partial \tilde{L}}{\partial \phi} = 0
   \end{split}
   \end{equation}
