==================================================
クリストッフェル記号を導出する
==================================================

.. warning::
   ちょっと長くなるけれど、頑張ればできるはず

前節で、ベクトル場の平行移動は上線付きの :math:`\overline{V}` で書くことにしたので、ここでもそう書くことにする。

:math:`x^{\mu} \to x^{\mu} + \mathrm{d}x^{\mu}` への平行移動の前後でベクトルの長さが
変わらないことを数式にすると以下のように表すことができる。

.. math::

   \abs{ V(x) }^{2}  = \abs{ \overline{V}(x + \dd{x}) }^{2}


これをメトリック :math:`g_{\mu \nu}` を使って書くと、次のようになる。

.. math::

   g_{\mu \nu}(x) V^{\mu}(x) V^{\nu}(x) = g_{\mu \nu}(x + \dd{x}) \overline{V}^{\mu}(x+\dd{x}) \overline{V}^{\nu}(x+\dd{x})


左辺はこれ以上計算する必要がない。
右辺を計算して、左辺と比べることで **クリストッフェル記号を導出** する。


右辺の計算に必要な要素
==================================================

まず、右辺の計算に必要な要素を、以下にリストしてみる。
4番目は、教科書に明記されてないけれど、使ってるはず。

#. :math:`g_{\mu \nu}(x + \dd{x}) = g_{\mu \nu}(x) + \partial_{\lambda} g_{\mu \nu}(x) \dd{x^{\lambda}}`
#. :math:`\overline{V}^{\mu} (x+\dd{x}) = V^{\mu}(x) - \Gamma^{\mu}_{\kappa \lambda} V^{\kappa}(x) \dd{x^{\lambda}}`
#. :math:`\overline{V}^{\nu} (x+\dd{x}) = V^{\nu}(x) - \Gamma^{\nu}_{\kappa \lambda} V^{\kappa}(x) \dd{x^{\lambda}}`
#. :math:`(\dd{x^{\lambda}})^{2}` 以上は、微小量なので無視する


:math:`\overline{V}^{\mu} \overline{V}^{\nu}` を計算する
================================================================================

(2)と(3)を使って :math:`\overline{V}^{\mu} \overline{V}^{\nu}` を計算する。
:math:`V(x)` の :math:`(x)` をは省略して書くことにする。

.. math::
   :nowrap:

   \begin{equation}
   \begin{split}
   \overline{V}^{\mu} \overline{V}^{\nu}
   & = \overline{V}^{\mu} (x+\dd{x}) \overline{V}^{\nu} (x+\dd{x})\\
   & = \pqty{ V^{\mu} - \Gamma^{\mu}_{\kappa \lambda} V^{\kappa} \dd{x^{\lambda}} }
       \pqty{ V^{\nu} - \Gamma^{\nu}_{\kappa \lambda} V^{\kappa} \dd{x^{\lambda}} }\\
   & = V^{\mu} V^{\nu}\\
      & \quad - V^{\mu} \pqty{ \Gamma^{\nu}_{\kappa \lambda} V^{\kappa} \dd{x^{\lambda}} }\\
      & \quad - V^{\nu} \pqty{ \Gamma^{\mu}_{\kappa \lambda} V^{\kappa} \dd{x^{\lambda}} }\\
      & \quad +
         \Gamma^{\mu}_{\kappa \lambda} V^{\kappa} \dd{x^{\lambda}}
         \Gamma^{\nu}_{\kappa \lambda} V^{\kappa} \dd{x^{\lambda}}\\
   & = V^{\mu} V^{\nu}
      - \pqty{ V^{\mu} \Gamma^{\nu}_{\kappa \lambda} } V^{\kappa} \dd{x^{\lambda}}
      - \pqty{ V^{\nu} \Gamma^{\mu}_{\kappa \lambda} } V^{\kappa} \dd{x^{\lambda}}\\
   & \quad + \order{ \dd{x^{\lambda}} }^{2} \qq{（微少量なので無視する）}\\
   & \sim
      V^{\mu} V^{\nu}
      - \pqty{ V^{\mu} \Gamma^{\nu}_{\kappa \lambda}
      + V^{\nu} \Gamma^{\mu}_{\kappa \lambda} } V^{\kappa} \dd{x^{\lambda}}\\
   \end{split}
   \end{equation}



:math:`g_{\mu \nu} \overline{V}^{\mu} \overline{V}^{\nu}` を計算する
================================================================================

:math:`g_{\mu \nu} \overline{V}^{\mu} \overline{V}^{\nu}` を計算する。
ここでも :math:`(x)` は省略して書くことにする。
また、前の計算結果の一部を (イ) と置き換えて計算する。

.. math::
   :nowrap:

   \begin{equation}
   \begin{split}
   \overline{V}^{\mu} \overline{V}^{\nu}
   & =
      V^{\mu} V^{\nu} - \mathrm{（イ）} V^{\kappa} \dd{x^{\lambda}}\\
      g_{\mu \nu} \overline{V}^{\mu} \overline{V}^{\nu}
   & =
      g_{\mu \nu} (x+\dd{x})
      \pqty{ V^{\mu} V^{\nu} - \mathrm{（イ）} V^{\kappa} \dd{x^{\lambda}} }\\
   & =
      \pqty{ g_{\mu \nu} + \partial_{\lambda} g_{\mu \nu} \dd{x^{\lambda}} }
      \pqty{ V^{\mu} V^{\nu} - \mathrm{（イ）} V^{\kappa} \dd{x^{\lambda}} }\\
   & =
      g_{\mu \nu} V^{\mu} V^{\nu}\\
      & \quad - g_{\mu \nu} \mathrm{（イ）} V^{\kappa} \dd{x^{\lambda}}\\
      & \quad + \partial_{\lambda} g_{\mu \nu} \dd{x^{\lambda}} V^{\mu} V^{\nu}\\
      & \quad - \partial_{\lambda} g_{\mu \nu} \dd{x^{\lambda}} \mathrm{（イ）} V^{\kappa} \dd{x^{\lambda}}\\
   & =
      g_{\mu \nu} V^{\mu} V^{\nu}
      - g_{\mu \nu} \mathrm{（イ）} V^{\kappa} \dd{x^{\lambda}}
      + \partial_{\lambda} g_{\mu \nu} \dd{x^{\lambda}} V^{\mu} V^{\nu}
      + \order{ (\dd{x^{\lambda}})^{2} }\\
   & \sim
      g_{\mu \nu} V^{\mu} V^{\nu}
      - g_{\mu \nu} \mathrm{（イ）} V^{\kappa} \dd{x^{\lambda}}
      + \partial_{\lambda} g_{\mu \nu} \dd{x^{\lambda}}  V^{\mu} V^{\nu}
   \end{split}
   \end{equation}

左辺と右辺の係数を比較する
==================================================

さて、ここで最初の **ベクトルの長さは平行移動しても変わらない** という条件に戻って **左辺と右辺の係数を比較する** 。
すると、条件式を新しい形で得ることができる。

.. math::

   - g_{\mu \nu}
   ( \text{イ} ) V^{\kappa} \mathrm{d}x^{\lambda}
   + \partial_{\lambda} g_{\mu \nu} \mathrm{d}x^{\lambda}
   V^{\mu} V^{\nu} = 0


（イ）の中身を代入して、 :math:`V^{\mu} V^{\nu} \dd{x^{\lambda}}` の形になるように整理する。

.. math::
   :nowrap:

   \begin{equation}
   \begin{split}
   - g_{\mu \nu} \pqty{ V^{\mu} \Gamma^{\nu}_{\kappa \lambda} + V^{\nu} \Gamma^{\mu}_{\kappa \lambda} } V^{\kappa} \dd{x^{\lambda}}
   + \partial_{\lambda} g_{\mu \nu} \dd{x^{\lambda}} V^{\mu} V^{\nu} & = 0\\
   - g_{\mu \nu} V^{\mu} \Gamma^{\nu}_{\kappa \lambda} V^{\kappa} \dd{x^{\lambda}}
   - g_{\mu \nu} V^{\nu} \Gamma^{\mu}_{\kappa \lambda} V^{\kappa} \dd{x^{\lambda}}
   + \partial_{\lambda} g_{\mu \nu} \dd{x^{\lambda}} V^{\mu} V^{\nu} & = 0\\
   - g_{\mu \nu} \Gamma^{\nu}_{\kappa \lambda} V^{\mu} V^{\kappa} \dd{x^{\lambda}}
   - g_{\mu \nu} \Gamma^{\mu}_{\kappa \lambda} V^{\nu} V^{\kappa} \dd{x^{\lambda}}
   + \partial_{\lambda} g_{\mu \nu} V^{\mu} V^{\nu} \dd{x^{\lambda}} & = 0
   \end{split}
   \end{equation}


ここで、第1項では :math:`\kappa \leftrightarrow \nu` の入れ替え、第2項では :math:`\kappa \leftrightarrow \mu` の入れ替え、を行う。

.. math::
   :nowrap:

   \begin{equation}
   \begin{split}
   - g_{ \mu \textcolor{red}{\kappa} } \Gamma^{ \textcolor{red}{\kappa} }_{ \textcolor{red}{\nu} \lambda} V^{\mu} V^{ \textcolor{red}{\nu} } \dd{x^{\lambda}}
   - g_{ \textcolor{red}{\kappa} \nu} \Gamma^{ \textcolor{red}{\kappa} }_{ \textcolor{red}{\mu} \lambda} V^{\nu} V^{ \textcolor{red}{\mu} } \dd{x^{\lambda}}
   + \partial_{\lambda} g_{\mu \nu} V^{\mu} V^{\nu} \dd{x^{\lambda}}
   & = 0\\
   \pqty{
   - g_{\mu \kappa} \Gamma^{\kappa}_{\nu \lambda}
   - g_{\kappa \nu} \Gamma^{\kappa}_{\mu \lambda}
   + \partial_{\lambda} g_{\mu \nu}
   }
   V^{\mu} V^{\nu} \dd{x^{\lambda}}
   & = 0\\
   \end{split}
   \end{equation}


恒等的に成り立つためには係数が0であればいいので

.. math::

   \partial_{\lambda} g_{\mu \nu}
   - g_{\mu \kappa} \Gamma^{\kappa}_{\nu \lambda}
   - g_{\kappa \nu} \Gamma^{\kappa}_{\mu \lambda} & = 0


こうして、やっと教科書p.125の式(9.29)が得られた。

.. note::
   ちなみに、ここまでで、目的の３分の２くらい。
   あともう少し。

接続 :math:`\Gamma` をメトリック :math:`g_{\mu \nu}` を使って表したいので、上で求めた式を以下のように工夫して組み合わせる。

（元の式） +（ :math:`\nu \leftrightarrow \lambda` を入れ替えた式） - （ :math:`\mu \leftrightarrow \lambda` を入れ替えた式）を計算する。


.. math::
   :nowrap:

   \begin{equation}
   \begin{split}
   \qq{（元の式）}
   \partial_{\lambda} g_{\mu \nu}
   - g_{\mu \kappa} \Gamma^{\kappa}_{\nu \lambda}
   - g_{\kappa \nu} \Gamma^{\kappa}_{\mu \lambda}
   & = 0\\
   \qq{（ $\nu \leftrightarrow \lambda$ を入れ替えた式）}
   \partial_{\nu} g_{\mu \lambda}
   - g_{\mu \kappa} \Gamma^{\kappa}_{\lambda \nu}
   - g_{\kappa \lambda} \Gamma^{\kappa}_{\mu \nu}
   & = 0\\
   \qq{（ $\mu \leftrightarrow \lambda$ を入れ替えた式） }
   \partial_{\mu} g_{\lambda \nu}
   - g_{\lambda \kappa} \Gamma^{\kappa}_{\nu \mu}
   - g_{\kappa \nu} \Gamma^{\kappa}_{\lambda \mu}
   & = 0
   \end{split}
   \end{equation}


各式の第１項はそのまま計算するしかない。
第２項と第３項は、同じ添字のメトリックで括るようにする。
（メトリックは対称テンソルなので :math:`g_{\kappa \lambda} = g_{\lambda \kappa}` ）



.. math::
   :nowrap:

   \begin{equation}
   \begin{split}
   \pqty{
      \partial_{\lambda} g_{\mu \nu}
      - \textcolor{red}{g_{\mu \kappa}} \Gamma^{\kappa}_{\nu \lambda}
      - \textcolor{blue}{g_{\kappa \nu}} \Gamma^{\kappa}_{\mu \lambda}
   }
   + \pqty{
      \partial_{\nu} g_{\mu \lambda}
      - \textcolor{red}{g_{\mu \kappa}} \Gamma^{\kappa}_{\lambda \nu}
      - \textcolor{orange}{g_{\kappa \lambda}} \Gamma^{\kappa}_{\mu \nu}
   }
   - \pqty{
      \partial_{\mu} g_{\lambda \nu}
      - \textcolor{orange}{g_{\lambda \kappa}} \Gamma^{\kappa}_{\nu \mu}
      - \textcolor{blue}{g_{\kappa \nu}} \Gamma^{\kappa}_{\lambda \mu}
   } & = 0\\
   \partial_{\lambda} g_{\mu \nu}
   + \partial_{\nu} g_{\mu \lambda}
   - \partial_{\mu} g_{\lambda \nu}
   - \textcolor{red}{g_{\mu \kappa}} \Gamma^{\kappa}_{\nu \lambda}
   - \textcolor{red}{g_{\mu \kappa}} \Gamma^{\kappa}_{\lambda \nu}
   - \textcolor{blue}{g_{\kappa \nu}} \Gamma^{\kappa}_{\mu \lambda}
   + \textcolor{blue}{g_{\kappa \nu}} \Gamma^{\kappa}_{\lambda \mu}
   - \textcolor{orange}{g_{\kappa \lambda}} \Gamma^{\kappa}_{\mu \nu}
   + \textcolor{orange}{g_{\lambda \kappa}} \Gamma^{\kappa}_{\nu \mu}
   & = 0\\
   \partial_{\lambda} g_{\mu \nu}
   + \partial_{\nu} g_{\mu \lambda}
   - \partial_{\mu} g_{\lambda \nu}
   - \textcolor{red}{g_{\mu \kappa}} \pqty{ \Gamma^{\kappa}_{\nu \lambda} + \Gamma^{\kappa}_{\lambda \nu} }
   - \textcolor{blue}{g_{\kappa \nu}} \pqty{ \Gamma^{\kappa}_{\mu \lambda} - \Gamma^{\kappa}_{\lambda \mu} }
   - \textcolor{orange}{g_{\kappa \lambda}} \pqty{ \Gamma^{\kappa}_{\mu \nu} - \Gamma^{\kappa}_{\nu \mu} }
   & = 0\\
   \end{split}
   \end{equation}


接続 :math:`\Gamma` も :math:`\nu \leftrightarrow \lambda` に対して対称（ :math:`\Gamma^{\mu}_{\nu \lambda} = \Gamma^{\mu}_{\lambda \nu}` ）であるので、

.. math::
   :nowrap:

   \begin{equation}
   \begin{split}
   \partial_{\lambda} g_{\mu \nu}
   + \partial_{\nu} g_{\mu \lambda}
   - \partial_{\mu} g_{\lambda \nu}
   - g_{\mu \kappa} \pqty{ \Gamma^{\kappa}_{\nu \lambda} + \Gamma^{\kappa}_{ \textcolor{red}{\nu} \textcolor{red}{\lambda} } }
   - g_{\kappa \nu} \pqty{ \Gamma^{\kappa}_{\mu \lambda} - \Gamma^{\kappa}_{ \textcolor{red}{\mu} \textcolor{red}{\lambda} } }
   - g_{\kappa \lambda} \pqty{ \Gamma^{\kappa}_{\mu \nu} - \Gamma^{\kappa}_{ \textcolor{red}{\mu} \textcolor{red}{\nu} } }
   & = 0\\
   \partial_{\lambda} g_{\mu \nu}
   + \partial_{\nu} g_{\mu \lambda}
   - \partial_{\mu} g_{\lambda \nu}
   - 2 g_{\mu \kappa} \Gamma^{\kappa}_{\nu \lambda}
   & = 0
   \end{split}
   \end{equation}

これを :math:`\Gamma^{\kappa}_{\nu \lambda}` について整理する。
左辺のメトリック :math:`g_{\mu \kappa}` を消去するには、その逆テンソル :math:`g^{\mu \kappa}` を、両辺の左から掛ければOK（行列の割り算みたいなもの）。

.. math::
   :nowrap:

   \begin{equation}
   \begin{split}
   2 g_{\mu \kappa} \Gamma^{\kappa}_{\nu \lambda}
   & =
   \partial_{\lambda} g_{\mu \nu}
   + \partial_{\nu} g_{\mu \lambda}
   - \partial_{\mu} g_{\lambda \nu}\\
   \Gamma^{\kappa}_{\nu \lambda}
   & =
   \frac{1}{2} g^{\mu \kappa}
   \pqty{
     \partial_{\lambda} g_{\mu \nu}
     + \partial_{\nu} g_{\mu \lambda}
     - \partial_{\mu} g_{\lambda \nu}
   }
   \end{split}
   \end{equation}

教科書に合わせるために :math:`\mu \leftrightarrow \kappa` を入れ替える。

.. math::

   \Gamma^{\mu}_{\nu \lambda} =
   \frac{1}{2} g^{\mu \kappa}
   \pqty{
     \partial_{\lambda} g_{\kappa \nu}
     + \partial_{\nu} g_{\kappa \lambda}
     - \partial_{\kappa} g_{\lambda \nu}
   }


これで、教科書p.126の式(9.33)が計算できた。
