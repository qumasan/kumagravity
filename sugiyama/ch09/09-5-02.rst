==================================================
メトリックと不変距離
==================================================

:math:`x^{\mu}` と :math:`x^{\mu} + \mathrm{d}x^{\mu}` の距離 :math:`\mathrm{d}s`

.. math::
   :nowrap:

   \begin{align}
   \mathrm{d}s^{2} & = g_{\mu \nu} \mathrm{d}x^{\mu} \mathrm{d}x^{\nu}
   \end{align}
