==================================================
【章末問題5.1】（教科書 p.79）
==================================================

.. todo::
   ローレンツ変換によって、２つのベクトル :math:`V^{\mu}, W^{\mu}`
   の内積が不変に保たれることを、ローレンツ変換の成分を具体的に用いて示せ。


.. note::

   上の例題5.2の計算の途中に出てくる以下の式に、
   ローレンツ変換の成分を具体的に代入して計算する。

   .. math::
      :nowrap:

      \begin{align}
      \eta_{\mu \nu} V'^{\mu} W'^{\nu}
      & = \eta_{\mu \nu} L^{\mu}_{\kappa} L^{\nu}_{\lambda} V^{\kappa} W^{\lambda}\\
      \end{align}

.. math::
   :nowrap:

   \begin{align}
   \eta_{\mu \nu} L^{\mu}_{\kappa} L^{\nu}_{\lambda} V^{\kappa} W^{\lambda}
   & = \eta_{0 0} L^{0}_{\kappa} L^{0}_{\lambda} V^{\kappa} W^{\lambda}
   + \eta_{1 1} L^{1}_{\kappa} L^{1}_{\lambda} V^{\kappa} W^{\lambda}
   + \eta_{2 2} L^{2}_{\kappa} L^{2}_{\lambda} V^{\kappa} W^{\lambda}
   + \eta_{3 3} L^{3}_{\kappa} L^{3}_{\lambda} V^{\kappa} W^{\lambda}\\
   & = - L^{0}_{\kappa} L^{0}_{\lambda} V^{\kappa} W^{\lambda}
   + L^{1}_{\kappa} L^{1}_{\lambda} V^{\kappa} W^{\lambda}
   + L^{2}_{\kappa} L^{2}_{\lambda} V^{\kappa} W^{\lambda}
   + L^{3}_{\kappa} L^{3}_{\lambda} V^{\kappa} W^{\lambda}
   \end{align}


ローレンツ変換 :math:`L^{\mu \nu}` の行列の成分は、

.. math::
   :nowrap:

   \begin{align}
   L^{\mu}_{\nu} &=
   \begin{pmatrix}
   L^{0}_{0} & L^{0}_{1} & L^{0}_{2} & L^{0}_{3} \\
   L^{1}_{0} & L^{1}_{1} & L^{1}_{2} & L^{1}_{3} \\
   L^{2}_{0} & L^{2}_{1} & L^{2}_{2} & L^{2}_{3} \\
   L^{3}_{0} & L^{3}_{1} & L^{3}_{2} & L^{3}_{3} \\
   \end{pmatrix}
   =
   \begin{pmatrix}
   \gamma  & - \gamma \beta  & 0 & 0 \\
   - \gamma \beta & \gamma & 0 & 0 \\
   0 & 0 & 1 & 0\\
   0 & 0 & 0 & 1\\
   \end{pmatrix}
   \end{align}


第１項の計算
--------------------------------------------------

とりあえず、マイナスを取った部分を計算する。

:math:`\textcolor{blue}{L^{0}_{2} = 0}, \textcolor{blue}{L^{0}_{3} =0 }` なので、
それを含む項はなくなることを考えると、
左上の４つの項だけが残る。

.. math::
   :nowrap:

   \begin{equation}
   \begin{split}
   L^{0}_{\kappa} L^{0}_{\lambda} V^{\kappa} W^{\lambda}
   & = \quad
     \textcolor{red}{L^{0}_{0} L^{0}_{0}} V^{0} W^{0}
   + \textcolor{red}{L^{0}_{0} L^{0}_{1}} V^{0} W^{1}
   + L^{0}_{0} \textcolor{blue}{L^{0}_{2}} V^{0} W^{2}
   + L^{0}_{0} \textcolor{blue}{L^{0}_{3}} V^{0} W^{3}\\
   & \quad
   + \textcolor{red}{L^{0}_{1} L^{0}_{0}} V^{1} W^{0}
   + \textcolor{red}{L^{0}_{1} L^{0}_{1}} V^{1} W^{1}
   + L^{0}_{1} \textcolor{blue}{L^{0}_{2}} V^{1} W^{2}
   + L^{0}_{1} \textcolor{blue}{L^{0}_{3}} V^{1} W^{3}\\
   & \quad
   + \textcolor{blue}{L^{0}_{2}} L^{0}_{0} V^{2} W^{0}
   + \textcolor{blue}{L^{0}_{2}} L^{0}_{1} V^{2} W^{1}
   + \textcolor{blue}{L^{0}_{2}} L^{0}_{2} V^{2} W^{2}
   + \textcolor{blue}{L^{0}_{2}} L^{0}_{3} V^{2} W^{3}\\
   & \quad
   + \textcolor{blue}{L^{0}_{3}} L^{0}_{0} V^{3} W^{0}
   + \textcolor{blue}{L^{0}_{3}} L^{0}_{1} V^{3} W^{1}
   + \textcolor{blue}{L^{0}_{3}} L^{0}_{2} V^{3} W^{2}
   + \textcolor{blue}{L^{0}_{3}} L^{0}_{3} V^{3} W^{3}\\
   & = \quad
     \textcolor{red}{ (\gamma) (\gamma) } V^{0} W^{0}
   + \textcolor{red}{ (\gamma) ( -\gamma \beta) } V^{0} W^{1}\\
   & \quad
   + \textcolor{red}{ ( -\gamma \beta) (\gamma) } V^{1} W^{0}
   + \textcolor{red}{ ( -\gamma \beta) ( -\gamma \beta) } V^{1} W^{1}\\
   & = \quad
     \textcolor{red}{ \gamma^{2} } V^{0} W^{0}
   + \textcolor{red}{ - \gamma^{2} \beta } V^{0} W^{1}
   + \textcolor{red}{ - \gamma^{2} \beta } V^{1} W^{0}
   + \textcolor{red}{ \gamma^{2} \beta^{2} } V^{1} W^{1}
   \end{split}
   \end{equation}


忘れないうちに、マイナスを付けておく、


.. math::
   :nowrap:

   \begin{align}
   - L^{0}_{\kappa} L^{0}_{\lambda} V^{\kappa} W^{\lambda}
   & = \quad
     \textcolor{red}{ - \gamma^{2} } V^{0} W^{0}
   + \textcolor{red}{ \gamma^{2} \beta } V^{0} W^{1}
   + \textcolor{red}{ \gamma^{2} \beta } V^{1} W^{0}
   + \textcolor{red}{ - \gamma^{2} \beta^{2} } V^{1} W^{1}
   \end{align}


第２項
--------------------------------------------------

同様に :math:`\textcolor{blue}{L^{1}_{2} = 0}, \textcolor{blue}{L^{1}_{3} = 0}` なので、
それを含む項はなくなることを考えると、４つの項だけが残る。

.. math::
   :nowrap:

   \begin{align}
   L^{1}_{\kappa} L^{1}_{\lambda} V^{\kappa} W^{\lambda}
   & = \quad
     \textcolor{red}{L^{1}_{0} L^{1}_{0}} V^{0} W^{0}
   + \textcolor{red}{L^{1}_{0} L^{1}_{1}} V^{0} W^{1}
   + L^{1}_{0} \textcolor{blue}{L^{1}_{2}} V^{0} W^{2}
   + L^{1}_{0} \textcolor{blue}{L^{1}_{3}} V^{0} W^{3}\\
   & \quad
   + \textcolor{red}{L^{1}_{1} L^{1}_{0}} V^{1} W^{0}
   + \textcolor{red}{L^{1}_{1} L^{1}_{1}} V^{1} W^{1}
   + L^{1}_{1} \textcolor{blue}{L^{1}_{2}} V^{1} W^{2}
   + L^{1}_{1} \textcolor{blue}{L^{1}_{3}} V^{1} W^{3}\\
   & \quad
   + \textcolor{blue}{L^{1}_{2}} L^{1}_{0} V^{2} W^{0}
   + \textcolor{blue}{L^{1}_{2}} L^{1}_{1} V^{2} W^{1}
   + \textcolor{blue}{L^{1}_{2}} L^{1}_{2} V^{2} W^{2}
   + \textcolor{blue}{L^{1}_{2}} L^{1}_{3} V^{2} W^{3}\\
   & \quad
   + \textcolor{blue}{L^{1}_{3}} L^{1}_{0} V^{3} W^{0}
   + \textcolor{blue}{L^{1}_{3}} L^{1}_{1} V^{3} W^{1}
   + \textcolor{blue}{L^{1}_{3}} L^{1}_{2} V^{3} W^{2}
   + \textcolor{blue}{L^{1}_{3}} L^{1}_{3} V^{3} W^{3}\\
   & = \quad
     \textcolor{red}{ (-\gamma \beta) (-\gamma \beta) } V^{0} W^{0}
   + \textcolor{red}{ (-\gamma \beta) (\gamma) V^{0} } W^{1}\\
   & \quad
   + \textcolor{red}{ (\gamma) (-\gamma \beta) } V^{1} W^{0}
   + \textcolor{red}{ (\gamma) (\gamma) V^{1} } W^{1}\\
   & = \quad
     \textcolor{red}{ \gamma^{2} \beta^{2} } V^{0} W^{0}
   + \textcolor{red}{ -\gamma^{2} \beta } V^{0} W^{1}
   + \textcolor{red}{ -\gamma^{2} \beta } V^{1} W^{0}
   + \textcolor{red}{ \gamma^{2} } V^{1} W^{1}
   \end{align}

第３項
--------------------------------------------------

:math:`\textcolor{blue}{L^{2}_{0} = 0}, \textcolor{blue}{L^{2}_{1} = 0}, \textcolor{blue}{L^{2}_{3} = 0}` なので、
:math:`L^{2}_{2}` だけの項が残る


.. math::
   :nowrap:

   \begin{align}
   L^{2}_{\kappa} L^{2}_{\lambda} V^{\kappa} W^{\lambda}
   & = \quad
     \textcolor{blue}{L^{2}_{0}} L^{2}_{0} V^{0} W^{0}
   + \textcolor{blue}{L^{2}_{0}} L^{2}_{1} V^{0} W^{1}
   + \textcolor{blue}{L^{2}_{0}} L^{2}_{2} V^{0} W^{2}
   + \textcolor{blue}{L^{2}_{0}} L^{2}_{3} V^{0} W^{3}\\
   & \quad
   + \textcolor{blue}{L^{2}_{1}} L^{2}_{0} V^{1} W^{0}
   + \textcolor{blue}{L^{2}_{1}} L^{2}_{1} V^{1} W^{1}
   + \textcolor{blue}{L^{2}_{1}} L^{2}_{2} V^{1} W^{2}
   + \textcolor{blue}{L^{2}_{1}} L^{2}_{3} V^{1} W^{3}\\
   & \quad
   + L^{2}_{2} \textcolor{blue}{L^{2}_{0}} V^{2} W^{0}
   + L^{2}_{2} \textcolor{blue}{L^{2}_{1}} V^{2} W^{1}
   + \textcolor{red}{L^{2}_{2} L^{2}_{2} V^{2} W^{2}}
   + L^{2}_{2} \textcolor{blue}{L^{2}_{3}} V^{2} W^{3}\\
   & \quad
   + \textcolor{blue}{L^{2}_{3}} L^{2}_{0} V^{3} W^{0}
   + \textcolor{blue}{L^{2}_{3}} L^{2}_{1} V^{3} W^{1}
   + \textcolor{blue}{L^{2}_{3}} L^{2}_{2} V^{3} W^{2}
   + \textcolor{blue}{L^{2}_{3}} L^{2}_{3} V^{3} W^{3}\\
   & = \quad
   \textcolor{red}{V^{2} W^{2}}
   \end{align}

第４項
--------------------------------------------------

第３項と同様に
:math:`\textcolor{blue}{L^{3}_{0} = 0}, \textcolor{blue}{L^{3}_{1} = 0}, \textcolor{blue}{L^{3}_{2} = 0}` なので、
:math:`L^{3}_{3}` だけの項が残る


.. math::
   :nowrap:

   \begin{align}
   L^{3}_{\kappa} L^{3}_{\lambda} V^{\kappa} W^{\lambda}
   & = \quad
     \textcolor{blue}{L^{3}_{0}} L^{3}_{0} V^{0} W^{0}
   + \textcolor{blue}{L^{3}_{0}} L^{3}_{1} V^{0} W^{1}
   + \textcolor{blue}{L^{3}_{0}} L^{3}_{2} V^{0} W^{2}
   + \textcolor{blue}{L^{3}_{0}} L^{3}_{3} V^{0} W^{3}\\
   & \quad
   + \textcolor{blue}{L^{3}_{1}} L^{3}_{0} V^{1} W^{0}
   + \textcolor{blue}{L^{3}_{1}} L^{3}_{1} V^{1} W^{1}
   + \textcolor{blue}{L^{3}_{1}} L^{3}_{2} V^{1} W^{2}
   + \textcolor{blue}{L^{3}_{1}} L^{3}_{3} V^{1} W^{3}\\
   & \quad
   + \textcolor{blue}{L^{3}_{2}} L^{3}_{0} V^{2} W^{0}
   + \textcolor{blue}{L^{3}_{2}} L^{3}_{1} V^{2} W^{1}
   + \textcolor{blue}{L^{3}_{2}} L^{3}_{2} V^{2} W^{2}
   + \textcolor{blue}{L^{3}_{2}} L^{3}_{3} V^{2} W^{3}\\
   & \quad
   + L^{3}_{3} \textcolor{blue}{L^{3}_{0}} V^{3} W^{0}
   + L^{3}_{3} \textcolor{blue}{L^{3}_{1}} V^{3} W^{1}
   + L^{3}_{3} \textcolor{blue}{L^{3}_{2}} V^{3} W^{2}
   + \textcolor{red}{L^{3}_{3} L^{3}_{3} V^{3} W^{3}}\\
   & = \quad
   \textcolor{red}{V^{3} W^{3}}
   \end{align}


全部足し合わせる
--------------------------------------------------


.. math::
   :nowrap:

   \begin{align}
   - L^{0}_{\kappa} L^{0}_{\lambda} V^{\kappa} W^{\lambda}
   & = \quad
     \textcolor{red}{ - \gamma^{2} } V^{0} W^{0}
   + \textcolor{blue}{ \gamma^{2} \beta } V^{0} W^{1}
   + \textcolor{blue}{ \gamma^{2} \beta } V^{1} W^{0}
   + \textcolor{red}{ - \gamma^{2} \beta^{2} } V^{1} W^{1}
   \\
   L^{1}_{\kappa} L^{1}_{\lambda} V^{\kappa} W^{\lambda}
   & = \quad
     \textcolor{red}{ \gamma^{2} \beta^{2} } V^{0} W^{0}
   + \textcolor{blue}{ -\gamma^{2} \beta } V^{0} W^{1}
   + \textcolor{blue}{ -\gamma^{2} \beta } V^{1} W^{0}
   + \textcolor{red}{ \gamma^{2} } V^{1} W^{1}
   \\
   L^{2}_{\kappa} L^{2}_{\lambda} V^{\kappa} W^{\lambda}
   & = \quad
   V^{2} W^{2}
   \\
   L^{3}_{\kappa} L^{3}_{\lambda} V^{\kappa} W^{\lambda}
   & = \quad
   V^{3} W^{3}
   \\
   & = (- \gamma^{2} + \gamma^{2} \beta^{2} ) V^{0} W^{0}
   + ( - \gamma^{2} \beta^{2} + \gamma^{2} ) V^{1} W^{1}
   + V^{2} W^{2}
   + V^{3} W^{3}
   \\
   & = - \gamma^{2} ( 1 - \beta^{2} ) V^{0} W^{0}
   + \gamma^{2} ( 1 - \beta^{2} ) V^{1} W^{1}
   + V^{2} W^{2}
   + V^{3} W^{3}
   \\
   &
   = - V^{0} W^{0}
   + V^{1} W^{1}
   + V^{2} W^{2}
   + V^{3} W^{3}
   \\
   & = \eta_{\mu \nu} V^{\mu} W^{\nu}
   \\
   \therefore \quad
   \eta_{\mu \nu} V'^{\mu} W'^{\nu} & = \eta_{\mu \nu} V^{\mu} W^{\nu}
   \end{align}


ということで、ローレンツ変換の成分を使った具体的な計算で、
内積がローレンツ不変であることを確認できた。
（労力に見合う計算だったかはともかく）
