==================================================
【例5.2】共変ベクトルの例（教科書 p.68）
==================================================

**反変ベクトルで微分した微分記号** は **共変ベクトル** である。
ということを、計算して確かめておく。

ある関数 :math:`u` を :math:`x'^{\mu}` で微分する。

微分のルールを使うと以下のようになる。

.. math::
   :nowrap:

   \begin{align}
   \frac{ \partial{u} }{ \partial{x'^{\mu}} } & =
   \frac{ \partial{x^{\nu}} }{ \partial{x'^{\mu}} }
   \frac{ \partial{u} }{ \partial{x^{\nu}} }
   \end{align}


右辺の係数部分が変換性を表す行列である。
これが :math:`L^{\mu}_{\nu}` なのか
:math:`\overline{L^{\mu}_{\nu}}` なのかを確かめればよい。

共変ベクトルの変換性とその微小変分の変換性を考える

.. math::
   :nowrap:

   \begin{align}
   x^{\mu} &= \overline{L^{\mu}_{\nu}} x'^{\mu}\\
   \mathrm{d}x^{\mu} &= \overline{L^{\mu}_{\nu}} \mathrm{d}x'^{\mu}
   \end{align}


割り算（のようなこと）をして、

.. math::
   :nowrap:

   \begin{align}
   \frac{ \mathrm{d}x^{\mu} }{ \mathrm{d}x'^{\mu} } &= \overline{L^{\mu}_{\nu}}\\
   \Rightarrow \quad
   \frac{ \partial{x^{\mu}} }{ \partial{x'^{\mu}} } &= \overline{L^{\mu}_{\nu}}
   \end{align}


ということで、係数は :math:`\overline{L^{\mu}_{\nu}}` 、
つまり **共変ベクトルの変換性と同じ** なことが分かった。


.. note::

   数学の先達に聞いたら **微分記号は共変** だが、 **微分して得られた量は元と同じ変換性** らしい。
   つまり、この場合、関数 u がスカラーなら :math:`\partial u / \partial x'^{\mu}` はスカラーになるということ。
