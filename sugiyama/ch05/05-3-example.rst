==================================================
【例5.3】４元速度（教科書 p.70）
==================================================

４次元座標の他の４元ベクトルを考えてよう、ということで
３次元の速度を拡張して４元速度を定義する。

まず、３次元の速度は、距離を時間で割ればいいので、以下のようになる。

.. math::
   :nowrap:

   \begin{align}
   v^{i} & = \frac{ \mathrm{d}x^{i} }{ \mathrm{d}t }
   \end{align}

これを、単純に４次元に拡張、つまり第０成分も含めて書く。
つまり、 :math:`i \to \mu` にする。

.. math::
   :nowrap:

   \begin{align}
   v^{\mu} & = \frac{ \mathrm{d}x^{\mu} }{ \mathrm{d}t }\\
   \end{align}


ただし、ローレンツ変換によって分母の :math:`\mathrm{d}t`
（＝言ってみれば :math:`t` の関数なので）も変換されてしまうため、
うまくいかない（共変性がなくなる）。
計算結果は :doc:`第4.4節 【例4.3】<./04-4>` （教科書 p.58）になる。

そこで **ローレンツ不変な時間：固有時間** :math:`\tau` を導入する。


.. math::
   :nowrap:

   \begin{align}
   u^{\mu} & = \frac{ \mathrm{d}x^{\mu} }{ \mathrm{d} \tau }
   \end{align}
