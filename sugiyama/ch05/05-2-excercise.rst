==================================================
【例題5.2】４元ベクトルの内積
==================================================

.. admonition:: 教科書p.67【例題5.2】

   4元ベクトル :math:`V^{\mu}` と :math:`W^{\mu}` の内積がスカラーであることを示す。
   つまり、

   .. math::
      :nowrap:

      \begin{align}
      V'^{\mu} \cdot W'^{\mu} & = V^{\mu} \cdot W^{\mu}
      \end{align}

   となるかどうかを確かめる。


まず、左辺を計算するために :math:`V^{\mu}` と :math:`W^{\mu}` をローレンツ変換する

.. math::
   :nowrap:

   \begin{align}
   V'^{\mu} & = L^{\mu}_{\nu} V^{\nu}\\
   W'^{\mu} & = L^{\mu}_{\nu} W^{\nu}
   \end{align}

上の変換式を内積の定義に代入すればいいのだが :math:`\nu` が **ダミー添字** であることに気をつける。
具体的には次のように書きなおして、代入に使うとよい。

.. math::
   :nowrap:

   \begin{align}
   V'^{\mu} & = L^{\mu}_{\kappa} V^{\kappa}\\
   W'^{\nu} & = L^{\nu}_{\lambda} W^{\lambda}
   \end{align}

内積の定義は :math:`V^{\mu} \cdot W^{\mu} \equiv \eta_{\mu \nu} V^{\mu} W^{\nu}` なので、

.. math::
   :nowrap:

   \begin{align}
   V'^{\mu} \cdot W'^{\mu}
   \equiv \eta_{\mu \nu} V'^{\mu} W'^{\nu}
   & = \eta_{\mu \nu} ( L^{\mu}_{\kappa} V^{\kappa} ) ( L^{\nu}_{\lambda} W^{\lambda} )\\
   & = \eta_{\mu \nu} L^{\mu}_{\kappa} L^{\nu}_{\lambda} V^{\kappa} W^{\lambda}\\
   & = \eta_{\kappa \lambda} V^{\kappa} W^{\lambda}\\
   & \equiv V^{\kappa} \cdot W^{\lambda} = V^{\mu} \cdot W^{\nu}\\
   \therefore \quad
   V'^{\mu} \cdot W'^{\mu}
   & =
   V^{\mu} \cdot W^{\mu}
   \end{align}

ということで、内積はスカラーであることが分かった。

最後の行の1つ前で :math:`\kappa \to \mu, \lambda \to \nu` という
添字の置き換えを行っているが :math:`V^{\kappa} \cdot W^{\lambda}` が表す内容は変わらないのでOKである。

どういうことかというと、アインシュタインの規約を展開して、次の計算をしているということ。

.. math::
   :nowrap:

   \begin{align}
   V^{\kappa} \cdot W^{\lambda}
   & = \eta_{\kappa \lambda} V^{\kappa} W^{\lambda}
   = \sum_{\kappa,\lambda=0}^{3} \left( \eta_{\kappa \lambda} V^{\kappa} W^{\lambda} \right)\\
   & = - V^{0} W^{0} + V^{1} W^{1} + V^{2} W^{2} + V^{3} W^{3}\\
   V^{\mu} \cdot W^{\nu}
   & = \eta_{\mu \nu} V^{\mu} W^{\nu}
   = \sum_{\mu,\nu=0}^{3} \left( \eta_{\mu \nu} V^{\mu} W^{\nu} \right)\\
   & = - V^{0} W^{0} + V^{1} W^{1} + V^{2} W^{2} + V^{3} W^{3}\\
   \therefore \quad
   V^{\kappa} \cdot W^{\lambda} & = V^{\mu} \cdot W^{\nu}
   \end{align}
