==================================================
【3.1】k計算法
==================================================

.. admonition:: 教科書

    p.25


.. important::


    次の計算結果を得るために頑張る

    .. math::
        :nowrap:

        \begin{align}
        k &= \frac{ \sqrt{1 + \beta} }{ \sqrt{1 - \beta} }
        \end{align}



これから考える条件をまとめる
==================================================

* ２つの慣性系 :math:`S` と :math:`S'` を考える
* この慣性系は相対速度が :math:`v` の関係にある
* 観測者A（ 慣性系 :math:`S` の住人）から観測者B（ 慣性系 :math:`S'` の住人）に向けて光信号を送信する。送信時間は :math:`T` とする。
* このとき、BはAからの信号を :math:`kT` という継続時間で受け取ると **仮定する** （＝時間が :math:`k` 倍になる）
* BからAに送る信号の時間もまた :math:`k` 倍になる（これも **仮定** )



時空図と原点O
==================================================

- 横軸 : :math:`x` （空間成分）、縦軸 : :math:`ct` （時間成分）
- 慣性系 :math:`S` の原点O : :math:`(x, ct) = (0, 0)`
- 慣性系 :math:`S'` の原点O : :math:`(x', ct') = (0, 0)`

慣性系 :math:`S` と 慣性系 :math:`S'` は原点Oで時刻を合わせておく（ :math:`t=0, t'=0` ）


AからBに光信号を送る
==================================================

まず、観測者Aになって考える。つまり慣性系 :math:`S` での時間経過を考える。
観測者AからBに信号を送り続ける時間を :math:`T` とする。
これを :math:`T_{A \to B} = T` と書くことにする。
Bに光信号を送り終えた時（＝時刻 :math:`T` の時）の観測者Aの場所を :math:`R` とすると、慣性系 :math:`S` の座標で :math:`R(0, cT)` と書くことができる。
線分 :math:`\overline{OR}` の長さは :math:`cT` となる。

.. math::
    :nowrap:

    \begin{equation}
    \begin{split}
    \overline{OR} &= cT_{A \to B}\\
    &= cT
    \end{split}
    \end{equation}


次に、観測者Bになって考える。つまり慣性系 :math:`S'` での時間経過を考える。
観測者BがAから光を受け続けた時間が :math:`T'` だったとする。
これを :math:`T'_{A \to B}` とする。
Aからの光信号を受け取り終えた時（＝時刻 :math:`T'`）の観測者Bの場所を :math:`P` とすると、慣性系 :math:`S'` の座標で :math:`P(0, cT')` と書くことができる。
線分 :math:`\overline{OP}` の長さは :math:`cT'` となる。

.. math::
    :nowrap:

    \begin{equation}
    \begin{split}
    \overline{OP} &= cT'_{A \to B}\\
    &= cT'
    \end{split}
    \end{equation}


ここで、最初の設定を思い出す。
:math:`T'=kT` と仮定したので線分 :math:`\overline{OP}` の長さは :math:`cT' = ckT` となる。

.. math::
    :nowrap:

    \begin{align}
    \overline{OP} &= ckT
    \end{align}



BからAに光信号を送り返す
==================================================

実は、観測者BはAからの光信号を随時送り返している。
最初の設定に書いてないじゃんと思うけど、とりあえず届いた光を鏡で反射しているようなイメージを持つことにする。

先ほどとは逆に、まず観測者Bになって考える。つまり慣性系 :math:`S'` での時間経過を考える。
観測者BがAに光を送り続ける時間は、Aからの光を受けていた時間に等しいので :math:`T'_{B \to A} = T'` である。


次に、観測者Aになって考える。つまり慣性系 :math:`S` での時間経過を考える。
Bから送り返された信号を観測者Aが受け取るのにかかる時間はk倍されるので :math:`T_{B \to A} = kT'_{B \to A} = kT'` となる。
この受け取り終えた場所を :math:`Q` とすると、慣性系 :math:`S` の座標で :math:`Q = (0, cT_{B \to A})` と書くことができる。
線分 :math:`\overline{OQ}` の長さは :math:`cT_{B \to A}` となる。

.. math::
    :nowrap:

    \begin{equation}
    \begin{split}
    \overline{OQ} &= cT_{B \to A}\\
    &= ckT'\\
    &= k^{2}cT
    \end{split}
    \end{equation}



三角形PQRを考える
==================================================

ここまでで設置した点R、P、Qを使ってできる三角形PQRを、慣性系 :math:`S` の座標で考える。
点R、点Qは :math:`S` 系の座標なので、そのままでOK。
点Pの座標は :math:`S'` 系の座標だったので、とりあえず :math:`S` 系の座標 :math:`(X, Y)` を適当に与えておく。

.. math::
    :nowrap:

    \begin{align}
    P &= (X, Y)\\
    Q &= (0, cT_{B \to A})\\
    R &= (0, cT_{A \to B})
    \end{align}

.. tip::
    Pの座標、適当すぎだけどいいの？思うかもしれないけど、後々の計算で消してしまうのでOK


ここからはほとんど図形の問題。
:math:`\triangle PQR` は 角Pが90°の直角二等辺三角形になっている。
この三角形の点Pから垂線を引き、線分QRとの交点をHとする。
点Pからの垂線なので、点Hの座標は :math:`H(0, Y)` と書くことができる。

線分RQを計算する
==================================================

.. admonition:: 教科書

    p.26 式(3.4)を確認する


直角二等辺三角形なので、最初の等式が成り立つ。

.. math::
    :nowrap:

    \begin{equation}
    \begin{split}
    \overline{RQ} &= 2 \overline{RH}\\
    &= 2 (\overline{OH} - \overline{OR})\\
    &= 2 (Y - \overline{OR})
    \end{split}
    \end{equation}


線分OQを計算する
==================================================

.. admonition:: 教科書

    p.26 式(3.5), (3.6)を確認する


上で計算した :math:`\overline{RQ}` を使って計算する。


.. math::
    :nowrap:

    \begin{equation}
    \begin{split}
    \overline{OQ} &= \overline{OR} + \overline{RH}\\
    &= \overline{OR} + 2 (Y - \overline{OR})\\
    &= \overline{OR} + 2Y - 2\overline{OR}\\
    &= 2Y - \overline{OR}
    \end{split}
    \end{equation}


Y について整理する。

.. math::
    :nowrap:

    \begin{equation}
    \begin{split}
    \overline{OQ} &= 2Y - \overline{OR}\\
    2Y &= \overline{OQ} + \overline{OR}\\
    Y &= \frac{1}{2} \left( \overline{OQ} + \overline{OR} \right)
    \end{split}
    \end{equation}


:math:`\overline{OR} = cT, \overline{OQ} = k^{2}cT` を代入する。


.. math::
    :nowrap:

    \begin{equation}
    \begin{split}
    Y &= \frac{1}{2} \left( \overline{OQ} + \overline{OR} \right)\\
    &= \frac{1}{2} \left( k^{2}cT + cT \right)\\
    &= \frac{1}{2} cT \left( k^{2} + 1 \right)
    \end{split}
    \end{equation}


線分PHを計算する
==================================================

.. admonition:: 教科書

    p.27 式(3.7)を確認する


.. math::
    :nowrap:

    \begin{equation}
    \begin{split}
    \overline{PH} &= \frac{1}{2} \overline{RQ}\\
    &= \frac{1}{2} \cdot 2(Y - \overline{OR})\\
    &= Y - \overline{OR}
    \end{split}
    \end{equation}

:math:`\overline{PH}` は 点Pの :math:`X` のこと。


.. math::
    :nowrap:

    \begin{equation}
    \begin{split}
    \overline{PH} &= Y - \overline{OR}\\
    X &= Y - \overline{OR}\\
    &= \frac{1}{2} cT \left( k^{2} + 1 \right) - cT\\
    &= \frac{1}{2} cT \left( k^{2} + 1 - 2 \right)\\
    &= \frac{1}{2} cT \left( k^{2} - 1 \right)
    \end{split}
    \end{equation}


直線OPの傾きを計算する
==================================================

.. admonition:: 教科書

    p.27 式(3.8)を確認する


直線OPの傾きは何だったかというと :math:`S` 系と :math:`S'` 系の相対速度 :math:`v` のことである。


.. math::
    :nowrap:

    \begin{equation}
    \begin{split}
    \frac{c}{v} & = \frac{Y}{X}\\
    & = \frac{ \frac{1}{2} cT \left( k^{2} + 1 \right) }{ \frac{1}{2} cT \left( k^{2} - 1 \right) }\\
    & = \frac{ k^{2} + 1 }{ k^{2} - 1 }
    \end{split}
    \end{equation}


kを計算する
==================================================

.. admonition:: 教科書

    p.27 式(3.9)を確認する

慣性系の間の相対速度を「光速の何割くらいの速さなのか」 で定義する。
:math:`\beta = 1` に近いほど光速に近づくと考えればいいので、相対論を考える上では非常に大事な置き換え。

.. math::
    :nowrap:

    \begin{align}
    \beta &= \frac{v}{c}
    \end{align}


.. math::
    :nowrap:

    \begin{align}
    \frac{1}{\beta} &= \frac{ k^{2} + 1 }{ k^{2} - 1 }
    \end{align}


:math:`k` について整理する。

.. math::
    :nowrap:

    \begin{equation}
    \begin{split}
    k^{2} - 1 & =  \beta ( k^{2} + 1 )\\
    k^{2} - 1 & =  \beta  k^{2} + \beta\\
    k^{2} - \beta  k^{2} &= 1 + \beta\\
    k^{2} ( 1 - \beta ) &= 1 + \beta\\
    k^{2} &= \frac{1 + \beta}{1 - \beta}
    \end{split}
    \end{equation}


:math:`k` は相対速度 :math:`v` で動いている慣性系同士の間での時間の延びを表す（と仮定した）ので :math:`k > 0` である。
ルートを外すときは正の符号を考えればOK。


.. math::
    :nowrap:

    \begin{align}
    k &= \sqrt{\frac{1 + \beta}{1 - \beta}}
    \end{align}


これで :math:`k` の値が相対論的な相対速度 :math:`\beta` に依存することが分かった。
このあとの例題を計算する時も、これをヒントにすればOK。



まとめ
==================================================


.. important::

    .. math::
        :nowrap:

        \begin{align}
        \text{相対速度} \quad \beta & = \frac{v}{c}
        \end{align}


    .. math::
        :nowrap:

        \begin{align}
        \text{時間の延び} \quad k &= \frac{ \sqrt{1 + \beta} }{ \sqrt{1 - \beta} }
        \end{align}
