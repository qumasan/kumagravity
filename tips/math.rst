==================================================
数式の挿入
==================================================

- インラインでは ``:math:`` ロール、ディスプレイでは ``.. math::`` ディレクティブを使用する
- 数式の入力にはLaTeXのマークアップを利用する
- マークアップにはLaTeXのAmSMathパッケージを利用している
- マークアップのガイドラインはOverleafのドキュメントを参照するとよさそう


.. code-block:: rst

    .. math:: R_{\mu \nu} - \frac{1}{2} R g^{\mu \nu} = \frac{8 \pi G}{c^{4}} T^{\mu \nu}

.. math:: R_{\mu \nu} - \frac{1}{2} R g^{\mu \nu} = \frac{8 \pi G}{c^{4}} T^{\mu \nu}



.. code-block:: rst

    .. math::
        :label:einstein

        R_{\mu \nu} - \frac{1}{2} R g^{\mu \nu} = \frac{8 \pi G}{c^{4}} T^{\mu \nu}

.. math::
    :label: einstein

    R_{\mu \nu} - \frac{1}{2} R g^{\mu \nu} = \frac{8 \pi G}{c^{4}} T^{\mu \nu}


.. code-block:: rst

    これがアインシュタイン方程式 :eq:`einstein` である


これがアインシュタイン方程式 :eq:`einstein` である

``math`` ディレクティブのオプション
==================================================

- 数式に番号をつけたい場合は ``:label:`` オプションを使う。このラベル名は ``:eq:`` ロール（もしくは ``:numref`` ）で指定することで数式番号を参照することができる
- ``:nowrap:`` をつけると自動的にラッピングされるの防ぐことができる。数式が長くなる場合（``make latexpdf`` で失敗する場合など）はこのオプションをつけるとよい。


複数行に渡る数式を整列させる
==================================================

教科書の数式を確認するために式変形を繰り返すと、あっという間に複数行にまたがる数式ができあがてしまう。
その時は ``split`` 環境を使うのがよさそう。
ただし ``split`` 環境は ``equation`` 環境の中で使わないといけない。

.. code-block:: rst

    .. math::
        :nowrap:

        \begin{equation}
        \begin{split}
        ここに数式を書く
        \end{split}
        \end{equation}


数式を整列させる方法について調べると ``eqnarray`` 環境を使った説明が見つかるが、だいぶ前に非推奨になっている。
複数行の数式を表示する環境には ``align`` 、 ``gather`` 、 ``split`` などがある。

また ``aligned`` や ``alignedat`` 環境というのも（いつの間にか）できているらしい。
これらも ``equation`` 環境の中で使う必要があることに注意する。
（ ``align`` 環境との違いはよく分かっていない）


参考サイト
==================================================

- https://www.sphinx-doc.org/ja/master/usage/restructuredtext/directives.html#math
- https://www.ams.org/arc/resources/amslatex-about.html
- https://www.overleaf.com/learn/latex/Mathematics
