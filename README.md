# くまのための相対論

- GitLab Repos : https://gitlab.com/qumasan/kumagravity/
- GitLab Pages : https://qumasan.gitlab.io/kumagravity/

# 執筆環境を整える

- GitLabリポジトリからクローンする
- ``poetry install`` を使って執筆環境（``Sphinx``）をセットアップする
- ``poetry shell``を使って執筆環境を起動する。すべての編集作業はこの仮想環境で実行する
- HTML出力／PDF出力ができるか確認する


```shell
$ git clone git@gitlab.com:shotakaha/kumagravity.git
$ cd kumagravity

## 執筆環境のインストール
$ poetry install

## 執筆環境を起動
$ poetry shell
(poetry) $ make html
(poetry) Build finished. The HTML pages are in _build/html.
(poetry) $ open _build/html/index.html

(poetry) $ make latexpdf
(poetry) pdflatex finished; the PDF files are in _build/latex.
(poetry) $ open _build/latex/KumaGravity.pdf
```

- `Sphinx v1.5` から `make latexpdf` でも `make latexpdfja` でもOKになった（自動判別してくれるようになった）

## Sphinxのテーマ

- 以前は ``sphinx_materialdesign_theme`` を使用していた
- 2020年末に ``sphinx_rtd_theme`` に変更した

## リポジトリの管理

- ``commitizen``を使ってコミットメッセージの管理をしている

```bash
$ git add コミットするファイル名
$ cz c
## commitizenのプロンプトが出てくるので、書いてあるとおりの内容を入力する
## 1. commitタイプを選択（矢印キーで選択）
## 2. 変更のスコープを入力【省略可】
## 3. 短いコミットメッセージ（全部小文字で書く／日本語でもOK）
## 4. 長いコミットメッセージ
## 5. BREAKING CHANGE かどうか？【デフォルトはNo】
## 6. フッタ（Breaking Changesの内容や、クローズするissue番号を入力する）
```

# GitLab CIの設定

- GitLab CI ドキュメントのSphinxサンプル（ https://gitlab.com/pages/sphinx ） をほぼそのまま使う
- ``image: python:3.9-alpine``に変更した
- 必要なパッケージを追加した（``poetry``のインストールに失敗するため、必要最低限のパッケージをインストール）
  - ``sphinx_rtd_theme``
  - ``toml``

# ブランチの作成

ブランチの作成は2通りある

1. リポジトリの``issue``から作成する
1. ローカルで作成する

## リポジトリの``issue``から作成した場合

```bash
$ git fetch
$ git branch -r
origin/issue-based-branch名
origin/master
...
$ git checkout issue-based-branch名
```

## ローカルでブランチを作成する

```bash
$ git branch new-branch名
$ git checkout new-branch名
$ git push --set-upstream origin new-branch名
```


# マージリクエストの作成

- ブランチをマージするときには、できるだけGitLab上でマージリクエストを作成するようにする

# タグを作成 & リポジトリに追加

- ``cz (commitizen)`` を使ってリポジトリの管理を行っている
- ``cz bump --changelog``を使って``CHANGELOG.md``の自動生成 & タグの作成を行う

``` shellsession
(poetry) $ cz bump --changelog
(poetry) $ git push -u origin --tags
```
