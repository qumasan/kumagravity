## 1.0.0 (2023-04-08)

### BREAKING CHANGE

- #14

### Feat

- **conf.py**: changed latex_engine to lualatex

### Fix

- **conf.py**: tomlに依存していた部分を削除した
- **conf.py**: ドキュメントのバージョン番号を修正した
- **conf.py**: added physics package to MathJax v3
- **topics/index.rst**: removed from toppage
- **topics/index.rst**: added 書籍の発売情報
- **ch09-1.rst**: 微分記号を修正
- **tips**: Gitの使い方を追記
- **tips**: math.rst を編集した
- **ch09**: 概要を編集
- **ch09**: クリストッフェル記号の導出を編集
- **ch09**: それぞれの節の最初にい重要な数式を挿入

### Refactor

- **conf.py**: Epub設定を整理した
- **conf.py**: texinfo設定を削除した
- **conf.py**: manpage設定を削除した
- **conf.py**: テキスト設定を整理した
- **conf.py**: LaTeX設定を整理した
- **conf.py**: HTML Help設定を整理した
- **conf.py**: HTML設定を整理した
- **conf.py**: 数式の設定を整理した（書き出した）
- **conf.py**: i18n設定を整理した
- **conf.py**: 全体設定を整理した
- **conf.py**: プロジェクト情報を整理した
- **conf.py**: LaTeXの設定項目を見直した
- **conf.py**: set latex_toplevel_sectioning to 'part'
- **conf.py**: remove extra spaces & comments

## 0.2.3 (2021-01-07)

### Fix

- **ch04**: replaced \mathrm{d} with \dd
- **ch05**: replaced \vec with \va

## 0.2.2 (2021-01-07)

### Fix

- **tips**: 説明を修正
- **html_theme_options**: set analytics_id

## 0.2.1 (2021-01-06)

### Fix

- **conf.py**: get version from pyproject.toml
- **conf.py**: LaTeX文書のタイトルを修正

## 0.2.0 (2021-01-06)

### Fix

- **conf.py**: disabled MathJax configuration
- **ch9**: add 09-6

### Feat

- **conf.py**: mathjax_config['extensions] = ['physics'] を追加
- **tips**: physicsパッケージの使い方を追記

## 0.1.3 (2021-01-05)

### Fix

- **ch09**: fix 09-2
- **ch09**: fix 09-5-08
- **ch09**: fix 09-5-05
- **ch11**: fix 11-2 (sec02)
- **ch09**: fix 09-3
- **ch09**: fix 09-5
- **ch09**: fix 09-4
- **ch05**: fix 05-2

## 0.1.2 (2021-01-05)

### Fix

- **changelog**: fix typo
- **changelog**: fix changelog
- **theme**: changed navigation_depth
- **tips**: add current theme setting
- **ch09**: edit 9-1, 9-3
- **ch05**: fix typo
- **tips**: add documentation config
- **tips**: split and add tips

## 0.1.1 (2021-01-05)

### Fix

- **theme**: changed navigation_depth
- **tips**: add current theme setting
- **ch09**: edit 9-1, 9-3
- **tips**: add documentation config
- **tips**: split and add tips

## 0.1.1 (2021-01-05)

### Fix

- **ch03**: まとめを追記
- **ch03**: 概要を編集
- **ch04**: 概要を編集

## 0.1.0 (2021-01-04)

### Feat

- **ch03**: 3.1節（k計算法）のまとめを追加

## 0.0.2 (2021-01-04)

### Fix

- **ch03**: add section 01
- **ch09**: moved to ch09
- **ch05**: moved to ch05
- **ch04**: moved to ch04
- **ch03**: moved to ch03
- **chapters**: add missing chapters
- **theme**: changed to sphinx_rtd_theme
- **package**: add physics

## 0.0.1 (2021-01-01)

### Fix

- **ch10**: 10.1節のtoctreeを追加
- **ch11**: ディレクトリ構成を変更した
- **ch10**: ディレクトリ構成を変更
- **toctree**: Warningを修正
- **ch03**: 時空図を使ったk計算法の説明を追記中
